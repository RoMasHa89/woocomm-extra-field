��    	      d       �       �      �      �      �      �   3   �   �        �  3   �    �        2        6  "   E  i   h    �  "   �  A   �   -- Additional Info: Alex Romantsov Enter something Please enter something into "Very important field". Plugin, that creates additional meta field "Additional Info" in WooCommerce Checkout page. Adds that field to E-mail and admin-side order form. Very important field WooCommerce additional meta field "Additional Info" Project-Id-Version: woocomm-extra-field.php conversion
Report-Msgid-Bugs-To: 
POT-Creation-Date: Thu Dec 10 2015 12:57:56 GMT+0200 (EET)
PO-Revision-Date: Thu Dec 10 2015 12:59:39 GMT+0200 (EET)
Last-Translator: 
Language-Team: 
Language: Unknown
Plural-Forms: nplurals=2; plural=n!=1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Loco-Source-Locale: en_UA
X-Loco-Parser: loco_parse_php
X-Loco-Target-Locale: zxx
X-Generator: Loco - https://localise.biz/ -- Дополнительная информация: Alex Romantsov Введите что-нибудь Пожалуйста, введите что-нибудь в поле "Оччень важное поле" Плагин, который добавляет дополнительное поле на страничках Checkout плагина WooCommerce. Также включает его значение в рассылку Email и в админ-кабинете. Оччень важное поле Дополнительное поле для WooCommerce Checkout 