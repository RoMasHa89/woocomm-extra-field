<?php
/*
Plugin Name: WooCommerce additional meta field "Additional Info"
Plugin URI: --
Description: Plugin, that creates additional meta field "Additional Info" in WooCommerce Checkout page. Adds that field to E-mail and admin-side order form.
Version: 1.0.0
Author: Alex Romantsov
Author URI: --
Text Domain: add_info_woocommerce
Domain Path: /languages
License: No Licence
*/


/**
 * Add fields to order
 */
function add_info_checkout_field( $checkout ) {

	echo '<div id="add_info_checkout_field"><h2>' . __('Additional Info:', 'add_info_woocommerce') . '</h2>';

	woocommerce_form_field( 'add_info_field_name', array(
			'type'          => 'text',
			'class'         => array('add-info-field-class form-row-wide'),
			'label'         => __('Very important field', 'add_info_woocommerce'),
			'placeholder'   => __('Enter something', 'add_info_woocommerce'),
	), $checkout->get_value( 'add_info_field_name' ));

	echo '</div>';

}
add_action( 'woocommerce_after_order_notes', 'add_info_checkout_field' );


/**
 * Update the order meta with field value
 */
function add_info_checkout_field_update_order_meta( $order_id ) {
	if ( ! empty( $_POST['add_info_field_name'] ) ) {
		update_post_meta( $order_id, 'add_info_field_name', sanitize_text_field( $_POST['add_info_field_name'] ) );
	}
}
add_action( 'woocommerce_checkout_update_order_meta', 'add_info_checkout_field_update_order_meta' );


/**
 * Display field value on the order edit page
 */
function add_info_checkout_field_display_admin_order_meta($order){
	echo '<p><strong>'. __('Very important field', 'add_info_woocommerce').':</strong><br> ' . get_post_meta( $order->id, 'add_info_field_name', true ) . '</p>';
}
add_action( 'woocommerce_admin_order_data_after_billing_address', 'add_info_checkout_field_display_admin_order_meta', 10, 1 );


/**
 * Process the checkout
 */
function add_info_checkout_field_process() {
	// Check if set, if its not set add an error.
	if ( ! $_POST['add_info_field_name'] )
		wc_add_notice( __( 'Please enter something into "Very important field".', 'add_info_woocommerce' ), 'error' );
}
add_action('woocommerce_checkout_process', 'add_info_checkout_field_process');


/**
 * Add custom fields to email keys
 */
function add_info_order_meta_key( $keys ) {
	$keys[__('Very important field', 'add_info_woocommerce')] = 'add_info_field_name';
	return $keys;
}
add_filter('woocommerce_email_order_meta_keys', 'add_info_order_meta_key');


// Localization
function add_info_plugin_textdomain() {
	load_plugin_textdomain( 'add_info_woocommerce', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'add_info_plugin_textdomain' );


// Our hooked in function - $fields is passed via the filter! Need to show our custom field in Order details page
function custom_override_order_fields( $order ) {

?>
	<header>
		<h2><?php _e('Additional Info:', 'add_info_woocommerce')?></h2>
	</header>
	<table class="shop_table shop_table_responsive customer_details">
		<tbody>
		<tr>
			<th><?php _e('Very important field', 'add_info_woocommerce') ?>:</th>
			<td><?php echo get_post_meta( $order->id, 'add_info_field_name', true ) ?></td>
		</tr>
		</tbody>
	</table>

<?php

	return $order;
}
// Hook in
add_action( 'woocommerce_order_details_after_order_table' , 'custom_override_order_fields' );


?>